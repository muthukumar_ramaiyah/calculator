package com.qa.calc;
/**
 * The selenium code launches calculator in browser, press buttons, and confirms results
 *
 * @author  Muthukumar Ramaiyah
 * @version 1.0
 * @since   2023-03-06
 */
public enum Buttons {
    zero("0"),
    one("1"),
    two("2"),
    three("3"),
    eight("8"),
    multiply("x"),
    subtract("-"),
    equals("=");
    final String buttonState;
    Buttons(String s) {
        this.buttonState = s;
    }

    public String getButtonState() {
        return this.buttonState;
    }
}
