package com.qa.calc;

import com.qa.CommonSteps;
import com.qa.fw.AbstractTest;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
/**
 * The selenium code launches calculator in browser, press buttons, and confirms results
 *
 * @author  Muthukumar Ramaiyah
 * @version 1.0
 * @since   2023-03-06
 */
public class CalcSteps extends AbstractTest {
    private IndexPage indexPage;
    public CalcSteps(CommonSteps commonSteps) {
        commonSteps.browser = browser;
    }
    @Given("Calc is launched")
    public void calcIsLaunched() {
        indexPage = new IndexPage(browser, properties.getProperty("calc.url"));
    }

    @When("OperandA: {string}, Operator {string}, and  OperandB: {string} are clicked")
    public void operator(String a, String operator, String b) {
        splitAndPush(a);
        indexPage.getButton(operator).click();
        splitAndPush(b);
        indexPage.getButton("=").click();
    }

    @Then("Validate result: {string} matches in display")
    public void validateResultResultMatchesInDisplay(String result) {
        String resultFromDisplay = indexPage.display.getAttribute("innerText");
        logger.info("Actual result in display: " + resultFromDisplay);
        Assert.assertEquals(result, resultFromDisplay);
    }

    private void splitAndPush(String num) {
        String[] numbers = num.split("");
        for (String n: numbers) {
            indexPage.getButton(n).click();
        }
    }
}
