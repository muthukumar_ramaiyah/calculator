## Requirements
```commandline
Chromedriver: makesure to match it with chrome browser version
https://chromedriver.storage.googleapis.com/index.html?path=98.0.4758.102/
mac users only
xattr -d com.apple.quarantine /Users/muthu/Downloads/chromedriver

java version 11
maven version 3.5.4
```
## NOTE
```commandline
This software framework tested only in mac os so far
```

## Usage
```commandline
Set path.to.chromedriver property  in dev.properties point to path of your chromedriver
mvn clean test -Droot.log.level=info -Dcucumber.filter.tags=@Calc
```

##cucumber-report
```commandline
Where to see reports: target/cucumber-html-reports/consolidated.html
Where to see test logs: logs/tests.log
```

####  Author
```commandline
Muthukumar Ramaiyah
linkedin: https://www.linkedin.com/in/muthukumar-ramaiyah/ 
```
